# jekyll-transit

A clean, minimal design that gets straight to the point.Built with app devs/startups in mind.

## Installation

Add this line to your Jekyll site's Gemfile:

```ruby
gem "jekyll-transit"
```

And add this line to your Jekyll site:

```yaml
theme: jekyll-transit
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install jekyll-transit

## Usage

	- To add services to your website, create `_services` folder under root directory. Create a `service-name.md` file inside `_services` folder. Write front matter inside post as follows: 
	```yaml
		---
		title: Consectetur adipisicing
		icon: fa-desktop
		icon_color: color9
		short_description: Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium ullam consequatur repellat debitis maxime.
		---

		CONTENT_HERE
	```
	- To add team members info, create `_team` folder under root directory.
	Create file name having `username.md` and include front matter in that file as
	```yaml
		---
		name: Voluptatem dicta
		username: dicta
		title: Et natus sapiente
		image: images/profile_placeholder.gif
		front_display: true
		---
	```

    - Update `social_links` in `_config.yml` file according to your social account url.


## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/hello. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## Development

To set up your environment to develop this theme, run `bundle install`.

You theme is setup just like a normal Jelyll site! To test your theme, run `bundle exec jekyll serve` and open your browser at `http://localhost:4000`. This starts a Jekyll server using your theme. Add pages, documents, data, etc. like normal to test your theme's contents. As you make modifications to your theme and to your content, your site will regenerate and you should see the changes in the browser after a refresh, just like normal.

When your theme is released, only the files in `_layouts`, `_includes`, and `_sass` tracked with Git will be released.

## License

The theme is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

